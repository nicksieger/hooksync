# -*- ruby -*-
require 'rubygems'
gem 'sinatra', '~> 0.9'
if defined? JRUBY_VERSION
  gem 'json_pure'
else
  gem 'json'
end
$LOAD_PATH.unshift "./lib"
require "hooksync"

set :run, false
set :environment, :production
set :views, File.dirname(__FILE__) + '/templates'
run Sinatra::Application
