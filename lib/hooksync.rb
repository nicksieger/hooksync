require 'sinatra'
require 'json'
require 'hooksync/revision'
require 'hooksync/repository'
require 'hooksync/config'

class Rack::Request
  # Hack around form_data? for this little app since we don't need any
  # form parameter request processing; JRuby-Rack doesn't support
  # rewindable 'rack.input', so if we don't get the right content-type
  # we don't have any request body to read.
  def form_data?
    false
  end
end

get '/' do
  @revisions = Hooksync::Revision.list
  erb :index
end

post '/' do
  hash = JSON.parse(request.body.read)
  Hooksync::Revision.add Hooksync::Revision.new(hash) if hash["event"] == "scm"
end

helpers do
  def h(str)
    require 'cgi'
    CGI::escapeHTML(str)
  end
end
