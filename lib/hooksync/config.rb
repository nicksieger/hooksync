require 'yaml'

module Hooksync
  class Config
    def self.config
      @config ||= {}
    end

    def self.repositories
      @repositories ||= []
    end

    def self.init
      @config.keys.each do |k|
        self.repositories << Repository.new(k, *ATTRS.map{|a| @config[k][a]})
      end
    end

    begin
      self.config.update(YAML::load(File.read(File.dirname(__FILE__) + '/../../config/repos.yml')))
      self.init
    rescue => e
      puts e.message
    end
  end
end
