module Hooksync
  ATTRS = %w(project feature url)
  class Repository < Struct.new(:name, *ATTRS.map{|a| a.to_sym})
    def self.find(project, feature)
      Config.repositories.detect {|r| r.project == project && r.feature == feature}
    end

    def do_callback(revision)
      Thread.new do
        begin
          revision.result = `ruby -S rake callback DATA=#{[JSON.generate(revision.data)].pack("m").gsub("\n",'')}`
        rescue Exception => e
          revision.result = %{#{e.message}\n#{e.backtrace.join("\n")}}
        end
      end
    end
  end
end
