module Hooksync
  class Revision
    attr_accessor :data, :project, :revision, :author, :repository, :message, :result

    def initialize(data)
      @data = data
      @project = data["project"]
      @revision = data["data"].last["revision"]
      @author = data["user"]
      @repository = Repository.find(@project, @data["feature"])
      @message = data["data"].last["message"]
      @repository.do_callback(self) if @repository
    end

    def revision_url
      if repository && repository.url
        %{<a href="#{repository.url}#{revision}">#{revision}</a>}
      else
        revision
      end
    end

    def time_ago_in_words       # poor man's action view
      time = DateTime.parse(@data["time"])
      diff = ((DateTime.now - time) * 86400).to_i
      case diff
      when 0..60
        "just now"
      when 61...(5 * 60)
        "a few minutes ago"
      when (5 * 60)...(50 * 60), (90 * 60)...(120 * 60)
        "#{diff / 60} minutes ago"
      when (50 * 60)...(90 * 60)
        "about an hour ago"
      when (120 * 60)...86400
        "#{diff / 3600} hours ago"
      when 86400...(2 * 86400)
        "1 day ago"
      when (2 * 86400)...(20 * 86400)
        "#{diff / 86400} days ago"
      else
        time.strftime "%A, %B %d, %Y at %H:%M"
      end
    end

    @lock = Mutex.new
    @revisions = []

    def self.list
      @lock.synchronize {
        @revisions.dup
      }
    end

    def self.add(r)
      @lock.synchronize {
        @revisions << r
        while @revisions.size > 10
          @revisions.shift
        end
      }
    end
  end
end
