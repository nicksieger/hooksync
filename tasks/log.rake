# Add other rakefiles in this directory with more callback task actions
task :callback do
  if CALLBACK_DATA["project"] =~ /hook/
    puts "Called with #{JSON.pretty_generate(CALLBACK_DATA)}"
  end
end
